﻿# Object Oriented Unified Data Structure

## Identified Roles in Data exchanges

Within supply chain data exchanges the roles identified from the Demonstrators and the theoretical analysis the core roles are:

* Customer

* Supplier/Seller

* Authority/ Customs

* Intermediary

In more detail through the various Demonstrators’ use cases the role of Customer was referred as:

1. Buyer

2. Invoicee

3. Payor

4. Importer

5. Consignee

The role of Supplier/Seller was met as:

1. Manufacturer

2. Invoicer

3. Payee

4. Exporter

5. Consignor

Authority/ Customs role was mentioned in the Demonstrators WPs as:

1. Other Government Agency

2. Port Health

3. Inspection

Finally, the role of Intermediary is reported as:

1. Transport Services Provider

2. Bank

3. Credit Agency

4. Customs Agency

The above terms that were finally used to describe all the different participants are in perfect alignment with the UN/CEFACT standard as it is presented in Figure 1-1. 

![Scheme](_images/image_0.png)

**Figure 1-1 Roles in CORE data exchanges (ref. UNCEFACT SDRM Data model)**

* A Seller is identified as an Original Consignor and/or Original Shipper. Seller is the party stipulated as the supplier of goods or services. The primary role of the Supplier as specified in the Sales Order Contract is the Seller and other possible roles include the Original Consignor/Shipper, Transport Services Buyer, Exporter, and Invoice Issuer.

* A Buyer is the party stipulated as the party to whom goods or services are sold. The primary role of the Customer as specified in a Sales Order Contract is the Buyer and other possible roles include the Final/Ultimate Consignee, Transport Services Buyer, Importer and Invoicee.

## Processes identified

The data exchanges between heterogeneous systems in the Supply Chain are categorized in the following aspects:

1. Procurement and Sales

    1. Establish Business Agreements

    2. Ordering

        1. Place Order

        2. Negotiate Order

        3. Cancel Order

        4. Demand forecast

        5. Inventory forecast

    3. Shipping

        6. Dispatch and Receive Goods

        7. Issue Information 

    4. Financial Processes 

        8. Issue Invoice

        9. Issue compensation

        10. Credit Notes

2. Logistic Processes (Maritime and Road)

    5. Book Transport Services

    6. Issue Shipping Information

    7. Confirm Receipt of Goods

    8. Exchange Dispatch Information

    9. Report Inventory Status (Road case)

    10. Book Wagon (Road case)

    11. Release Goods

    12. Check Status Information

3. Regulatory Processes

    13. Customs Export 

        11. Declare Goods Export

        12. Declare Cargo Export

        13. Send Risk Assessment

        14. Release Goods

        15. Provide Cargo Export Information

        16. Inform whoever responsible (other government agency-OGA)

    14. Customs Import

        17. Declare Goods Import

        18. Declare Cargo Import

        19. Inform other government agency of import

        20. OGA Provide risk assessment

        21. Release goods

    15. Inform Waste Movement

        22. Notify on Waste Receipt and Disposal

        23. Cancel Waste Transport

## High level entity relationship model

The following figure depicts a Master Data Exchange Structure which is a collection of information structured in such a way that it covers the data exchange structures required by users within the Reference Data Model domain (UNCEFACT, SDRM data model), such as the Supply Chain. From the Master Data Exchange Structure different Business Data Exchange Structures can be derived. A Business Data Exchange Structure is a collection of information used within a particular business process, structured in such a way that it covers the business data exchange needs. These structures can be a complete business document, such as an invoice or a mini document (snippet) as a result of a query e.g. on master data.

![Scheme](_images/image_1.png)

**Figure 1-2 High level entity relationship diagram (ref. UNCEFACT SDRM Data model)**

## SCSRF Data model

Due to the close collaboration with UNCEFACT (as partners within CORE) and their work under WP10, the design and composition of the common data exchange structure was heavily influenced. Looking for a generic and standardized way of supporting data exchanges among heterogeneous systems, UNCEFACT has provided their expertise and knowledge to construct a common data model. 

Furthermore, to the high-level entity relationship diagram presented in the previous section the main entities of the common data exchange structure are presented below.

        1. Master Consignment Entity

 A consignment is a separately identifiable collection of Consignment Items (available to be) transported from one Consignor to one Consignee via one or more modes of transport as specified in one single transport service contractual document.

1. A Consignment can only have one Transport Service Buyer

2. A Consignment can only have one Transport Service Provider

3. A Consignment can only have one Consignor

4. A Consignment can only have one Consignee

5. The Transport Service Buyer can be either the Consignor or the Consignee

6. A Consignment is made up of one or more Consignment Items

7. A Consignment can be made up of some or all Trade Items (aggregated into Consignment Items) from one or more Shipments

8. A Consignment is made up of one or more Customs Items for reporting to Customs

9. A Consignment can have one or more Customs UCRs (Unique Consignment Reference)

![Scheme](_images/image_2.png)

**Figure 1-3 Master Consignment entity**

A separately identifiable collection of goods items to be transported or available to be transported from one consignor to one consignee via one or more modes of transport where each consignment is the subject of one single transport contract.

Figure 1-4 depicts the Consignment Item entity which is a separately identifiable quantity of products grouped together by Customs code or packaging for transport purposes. A Consignment Item is the lowest level of information within a Consignment. An item within a consignment of goods separately identified for transport or regulatory purposes.

![Scheme](_images/image_3.png)

**Figure 1-4 Consignment Item entity**

An important entity that ensures and establishes the exchange of data between the Seller and the Buyer is the Trade Agreement (Figure 1-5). This entity characterizes the relationship between the two roles and is necessary as information for the regulatory procedures during Customs monitoring over the various orders.

![Scheme](_images/image_4.png)

**Figure 1-5 Trade Agreement Entity**

The whole data model can be summarized and represented in the following figure and it is aligned with the UNCEFACT standard data exchange model.

![Scheme](_images/image_5.png)

**Figure 1-6 SCSRF Data-model Overview, aligned with UNCEFACT concepts**

