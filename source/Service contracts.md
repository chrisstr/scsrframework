## Services and data contracts to be used for implementing data exchange processes between various SC stakeholders

The CORE Ecosystem, is deployed as a federation of Supply Chain Community Nodes utilising a common Infrastructure Technology and a number of tools and added value services (i.e. Enablers) for interoperability and sharing of information and other digital resources between the Ecosystem Participants. The Ecosystem enables Community Node Participants. to publish and to subscribe to information pertinent to operating supply chains in an efficient and secure manner whilst supporting Customs and other authorities to conduct effective supervision of global trade. Further, Community Node Participants can publish and subscribe information. Therefore, the CORE Ecosystem consists of and is the sum of the Ecosystem – Registered Nodes (SCC Ns) which enable supply chain business communities to share information and to utilise the Ecosystem Technology Stack Components.

Figure 5-1 shows the individual high-level services that a Node needs to implement so that it is secure, reliable and offers added value to the Participants. As stated in detail in T7.1 and its both deliverables (D7.11 and D7.12) the following groups of services are composing the Trade Facilitation and Logistics Services which are standardized services offered by a node to the end-users. 

![image alt text](_images/image_6.png)

**Figure 5-1 Set of services**

The services are explained thoroughly in D7.12 with respect to Demonstrator use cases.

**Flow of selecting a service contract-example**

Logistic Service Registry (Role: Carrier) → Service Specification (Service Name: Container Tracking) → Service Properties (Type: Container)

**1. Interoperability Services**

The optimisation services constitute a set of data exchange services, which provide the ability to a set of Supply Chain Management (SCM) related systems, to exchange information, as thoroughly described in D5.12 (NOTE:  D5.12- Optimization Services and External APIs, Version 2).

A generic mechanism for generating an API of APIs that realises the seamless interoperability among the SC systems was developed, avoiding the exhaustive implementation of the various APIs of all external and internal systems involved in the demonstrators. The initial approach presented in D5.11 was extended and completed so that the API of APIs incorporates all covered systems.

The Optimisation Services are fed with data from the SC Situational Awareness Maps developed in WP4 (especially in Task 4.4). The domain model that is being used is based on the SCSRF domain model. In addition, the Library uses the Connectivity Infrastructure developed in WP7 to interact via the proposed APIs with the external and internal systems. The developed optimisation service interfaces have a direct interaction with all demonstrators WPs (WP9 – WP17) as they facilitate the interaction with all internal CORE systems and LARG+O implementations, in addition to external systems and the seamless interoperability between them. Thus, the developed Library of Optimisation Services complements Task 8.2 and adds the relevant capabilities to the CORE Ecosystem developed in WP8.

**2. Data Contracts**

Access to some services may be restricted. In that case, there is a need to record the inter-participant agreements (Service Agreements) regarding a service usage. These agreements are stored in a Service Agreement repository and regulate the sharing of information in the CORE Ecosystem. The CORE Ecosystem infrastructure services support establishing and enforcement of Service agreements. The Service agreement is the formal contract that controls how information is exchanged/shared amongst the SCCN participants. The Data Access Control component is used to enforce the agreed Data Access Policies and/ or Service Agreements so that only agreed data and services an accessed.

