.. SCSRFramework documentation master file, created by
   sphinx-quickstart on Thursday March 8 11:40:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SCSRFramework Documentation!
=========================================

Contents
---------------

.. toctree::
   :maxdepth: 3

   Meta model of architectural data-part1
   OODS
   Service contracts

Indices and tables
---------------

* :ref:`genindex`
