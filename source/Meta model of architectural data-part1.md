1. Meta model of architectural data

## Introduction 

CORE applies model driven development to let services be quickly defined and deployed. The CORE uses metamodels as a link all architecture levels, linking: 

1. Business Architecture Perspectives which link Business Goals and Objectives, Business Rules and Constraints, Supply Chain Definition of merchandise flows, Stakeholders and Roles to physical Supply chain structures, as ports and connections (multimodal), to 

2. Data and Information Architecture Perspectives, including dashboards, collaborations and information exchanges, business and supply chain events, compliance and reporting, to 

3. the Technology Infrastructure Architecture Perspectives which involve the designs of the governance and infrastructure security, the deployment of application services in the services-oriented architecture of CORE, the publish – subscribe and the events management systems implementations, the technology implementation of the pipeline components, and their access and configuration.

The CORE metalevel definitions of business and implementation concepts have the aim to enable collaboration and dashboards related development and configurations (in the Living Labs) to be performed at a high level of abstraction, consistently across domains (and Living Labs) and through the CORE ecosystem.

Metamodels *are models* [1] that describe the semantic, structural and behavioural features of models, defining the basic constructs and elements of a domain to populate these models, thereby, establishing a standard modelling system for various domains. More simply, a metamodel describes all the concepts that are used in a model, hence, it is *a model of models*, *a language* to define models.

The CORE SCSRF aligns with the OMG (Object Management Group) approach to Model Driven Development (MDD) and Model Driven Architectures (MDA), developing models and model artifacts for the systems and applications developed and deployed in the CORE Ecosystem (D8.1), and the CORE Living Labs. 

In that sense, the defined models in CORE are an important asset of the CORE deliveries, formally defining the developed systems and resources in the form of models, either these being Architectural and / or Infrastructure Technology Models (in WP7 and WP8), Applications (in WP3, WP4 and WP5), or Business Models and Collaborative Processes enabled by the above, which drive, define and verify the Technologies and Applications developments which reside (are implemented) in the Living Labs Work-Packages. 

Therefore, CORE allows for more than one model types, aimed at different recipients - ‘personas’, which range from Business and Domain experts and Business Users in Supply Chain, to Architects and Developers of the solutions and infrastructure components. 

Business, Software Applications and Technology Architecture Models provide and are assessed by their utility and their achieved level of:

1. *Abstraction* delivering a picture of the system they represent, 

2. *Understandability*, following a consistent and globally accepted notation to highlight the modelled system aspects, 

3. *Accuracy*, conveying dependable process, data and behaviour information about the modelled systems, (d) *Completeness*, being comprehensive in their descriptions about the modelled systems, 

4. *Generality*, able to form the basis for reuse and to extend their usefulness covering a wider perspective to the one that has initiated their structure, 

5. *Actionability* and *Realisation* potential, being able to be transformed either to software artifacts enabling the modelled processes via e.g. a code – system generation transformation, or able to be e.g. simulated and thereby facilitate the potential to predict outcomes, KPIs etc. of modeled processes and functions. 

The above set of criteria is a set of best practices for adoption and compliance during the modelling actions in CORE, while practicing the corresponding supported modeling formalisms, and the metamodels constructions of the CORE SCSRF. 

SCSRF is considering Metamodels as a key concept in the Model Driven approach which is pursuing,  creating specification models for Applications, Infrastructures, Living Labs definitions and all systems under study, where each development is delivering in sequence models conforming to the adopted modeling approaches, in Modelling all Enterprise level Architectures of the CORE ecosystem and the Living Labs configurations (using the Archimate modeling language for the system and solutions descriptions), and the detailed models of Processes (Use Cases), Behaviours (Activity and Interaction  

## Approach / Kind of a literature Survey of metamodeling approaches

### Metamodels and the four-layer architecture.

As described in the previous section, the CORE metamodels may comprise of different system views, and will certainly aggregate different sets of users and uses, balancing between the business-related viewpoints to the architecture and the implementation relevant viewpoints, these being set in different levels of abstraction and handling / covering different aspects of the modelled domain mapping over the models’ landscape.

CORE uses the Metamodeling approach in to grouping concepts in ‘within’ models, that belong the same modelling approach or method (i.e. UML, BPMN, Archimate) and into ‘cross’-models that belong on to different families of methods and techniques, which however, are relevant to the specific analysis or implementation/deployment phase abstracted each time. 

This extended coverage in terms of methods and representations for knowledge elicitation has been necessitated by the fact that CORE is not merely developing applications, but also requires models to capture and relate Strategies, Business Objectives, Physical Movements of merchandise for Risks, Installations, Infrastructure components such as the pipelines, pub-sub, event processing components etc., which are in turn linked to innovative applications for tracking and tracing, information fusion and risks management. 

Therefore, the SCSRF modeling approach supports the construction of models to communicate information to both business users, systems and information architects and developers. The metamodels structure enables to have all these models cross-reference, connect and link together, so composite snapshots of the reality, combining e.g. Infrastructure Architectural constructs and top-level business Goals can be crafted in ArchiMate, while, processes link to use cases, down to Object Collaboration diagrams and Class diagrams in UML, creating detailed specifications to be taken up by the WP7 tooling applications. 

The ‘semantic’ linking of modelling concepts is served via enforcing consistency in terminology use and the Ontologies and Knowledge Graphs components and resources of CORE.

Hence, in this way, the SCSRF metamodels of CORE are taking the metamodeling action a step further, by linking all similar concepts across methodologies, thereby highlighting and at the same time unifying the different views in and between the different models, based on their ‘pragmatic’ interpretation.

The metamodeling framework of CORE is based on a modeling architecture comprising of four layers (see Figure 1.1) as adopted by OMG [2] ; that is the meta-metamodel (M3), metamodel (M2), model (M1) and real world concepts (M0) as follows:

![image alt text](_images/image_1.1.png)

**Figure 1-1 Four Layers Architecture**

* At the (M0) level there exist the real-world concepts.

* A Model (M1) is capturing and is modeling real world objects and artifacts, complying to the descriptions, and using the syntax of a textual or graphical ‘language’, described at the meta-modeling level (M2) and by the corresponding metamodel.  

* A Metamodel (M2) is an explicit model of the constructs and rules needed to create models, defined in a textual or graphical language and can be considered as the model of a modeling approach used. The introduced metamodels in CORE are Archimate, BPMN, UMM, and UML, as significant part of the model definitions of the Living Labs are done in UML Use Cases, Class and Object Models, and Interaction Diagrams.

* A Meta-metamodel is used to define the metamodels. The Meta-metamodel (Μ3) level of CORE has been based in the Meta Object Facility (MOF) of OMG and has been introduced for reasons of completeness. MOF uses a subset of the UML 2.2 core to define its modelling framework. The four main modeling concepts are classes, which model MOF metaobjects; associations, which model binary relations between metaobjects; MOF is mostly covering the UML Metamodels (UMM, and UML) of the (M2) level, and the constructs used at that level such as e.g. the Class diagrams and OCL constraints.

### Ecosystem Modelling Metamodels

#### The ArchiMate Metamodeling Level (M2).

ArchiMate models are used to describe the data and information, services and organization aspects of the Ecosystem, including the foreseen Business Services, Processes and Collaborations (the Business Layer) supporting the project innovations, visions and goals (the Motivation layer), implemented by the deployed Application Services, Processes and Functions in the CORE services oriented architecture, supported by all infrastructural components of the CORE ecosystem such as the Pipelines, the Pub-Sub services, the security and governance services implementation, and the Events based Architecture functionality (see Figure 1.2). 

![image alt text](_images/image_1.2.png)

**Figure 1-2: Motivation - Strategy - Core Elements Metamodels**

Archimate provides all modeling concepts to generate comprehensive ‘enterprise’ level architecture descriptions, which are serving properly the knowledge elicitation requirements addressed at a specific ‘viewpoint’ (e.g. Motivation, Strategy, Business, Application, Technology, Physical and Project) while at the same time provides the links to describe ‘realisation’ patterns and to describe connecting relations between concepts on different layers, hence, to link requirements to objectives, to outcomes and implementation actions, to services, applications and deployed technology components (see Figure 1.3 (NOTE:  Source: R&A Enterprise Architecture, https://ea.rna.nl/2016/09/09/archimate-3-0-metamodel-pdf/) (NOTE:  Archimate Reference Specification: http://pubs.opengroup.org/architecture/archimate3-doc/)).  ArchiMate covers different representation aspects of a modelled architecture:

* **Motivation Aspect** modelling the motivations, or reasons, that guide the design or change of an Enterprise Architecture.

* **Strategy Aspects** modeling the actions required to lead to outcomes and to meet with the requirements of the motivation level, linked to the available organisational capabilities and resources.

* Core Modelling Aspects comprising of:

    * **Active Structure Aspect**, representing the structural elements, e.g. the business actors, application components, and devices that display actual behaviour (the **_who_** or **_subject_**; structural elements are assigned to behavioural elements, to show **_who_** or what displays the behaviour).

    * **Passive Structure Aspect**, usually information objects in the Business Layer and data objects in the Application Layer, but they may also be used to represent physical objects (the **_what _**or **_object, _**which represent the objects on which specific behaviour is performed).

    * **Behaviour Aspect**, representing the behaviour e.g. processes, functions, events, and services performed by the actors. (the **_how_** or **_verb_**)

![image alt text](_images/image_1.3.png)

**Figure 1-3: Model Elements; Business, Application and Technology Layers**

The metamodel of ArchiMate (see Appendix A.2 for complete set of ArchiMate metamodels), combines *Behaviour Elements* (External and Internal, Event, Capability and Course of Action) and *Structural* *Elements* (Active – External and Internal, and Passive elements and Resources). 

Figure 1.4 shows the External and Internal behaviour Elements in ArchiMate, as well as their relationships for the Business layer. All the ArchiMate Architecture composition elements are shown in Appendix A.1.

As already stated, the metamodel considers that Active Structure elements are able to perform behaviour, being *Internal* *Active Structure* *Elements* (Business Actors, Components, etc.) and *External Active Structure Elements *(Interfaces that expose behaviour). The natural language analogy of the Behaviour elements is that of a *Verb*, while Active Structure Elements are analogous to ‘nouns’, taking the role of the *Subject *in a phrase, and the Passive Structure Elements are still ‘nouns’ with the role of an *Object* in a phrase. 

![image alt text](_images/image_1.4.png)

**Figure 1-4: Active and Passive Elements Metamodel for the Business Layer**

At the motivation layer, the concepts captured are the ones which *drive *the business services and processes establishment, defining and providing answers to the "Why" question of the Zachman [3] framework.  The Motivation level metamodel ( Figure 1.5) shows all the motivation level elements and constructs (explained in Appendix A.1, Table 2-2). The motivation elements are: stakeholder, value, meaning, driver, assessment, goal, outcome, principle, and requirement, while constraint is modelled as a subtype to requirement.

![image alt text](_images/image_1.5.png)

**Figure 1-5: Motivation Level Metamodel**

The Business Layer Active entities (Figure 1.6) are Business Actors or Business Roles, and Business Collaborations, that is entities that perform behaviour such as Business processes, Business functions or Business Interactions. Business interfaces are the channels where the services are offered. Descriptions of the Model Elements in this layer are given in appendix A.1, Table 2-3: Business Layer Elements.

![image alt text](_images/image_1.6.png)

**Figure 1-6: Application Layer Metamodel**

![image alt text](_images/image_1.7.png)

**Figure 1-7: Business Layer Metamodel**

The Application Layer metamodel supports (Figure 1.7) the creation of models of information and application architectures as defined by the TOGAF framework [4], describing the structure and behaviour characteristics of applications. The active structural elements of the model are the *application components*. The passive structural elements are the *data objects*. The behaviour elements are Application Functions, Application Processes, Application Interactions, Application Services and Events. Interrelationship between components is covered via Application Collaborations. Application Interfaces cover the communication with the environment and provide the route/channel for the services to be accessed. The definitions of the modelling elements of the Applications metamodel are in Appendix A1, in " Table 2-4: Application Layer Elements”.

![image alt text](_images/image_1.8.png)

**Figure 1-8: Technology Layer Metamodel**

The Technology layer metamodel (Figure 1.8) defines all the concepts and their relationships for the constructs used to model the technology architecture of an enterprise, and in the case of CORE the ecosystem components. As in the TOGAF framework, the technology components in Archimate are modelling the Structure and the interactions of the infrastructural services (at the platform level), and the logical and physical components comprising the infrastructure. The main Active structure element of the technology layer is the node, while the technology interface is for the purpose of modelling the offered platform services to other components or the components of the applications layer. The definitions of Elements in the Technology Layer are in Appendix A1, in "Table 2-5: Technology Layer Elements".

The Physical Layer metamodel (Figure 1.9) is comprising of components to model physical elements such as the Equipment and Facilities which are active structure elements, Equipment, Facilities, and Distribution Networks, and materials for the Passive Structure Elements. The behaviour elements of the Physical Layer metamodel are shared with the Technology Layer, and they are Technology Processes, Functions, Services and Events. 

The relationships metamodel (Figure 1.10: The Relationships Metamodel) involves Structural, dynamic, and Dependency relationships, as well as the inheritance (specialization) and the Association Relationships.

![image alt text](_images/image_1.9.png)

**Figure 1-9: Physical Layer Metamodel**

![image alt text](_images/image_1.10.png)

**Figure 1-10: The Relationships Metamodel**

#### The UMM Meta modelling Level (M2).

UMM (UN/CEFACT’s Modeling Methodology) is a methodology that originates in the United Nation’s Centre for Trade Facilitation and Electronic Business (UN/CEFACT), which has developed EDIFACT and ebXML.[5]. 

UMM fits well with the Model Driven Development (MDD) and Model Driven Architecture concepts of CORE, separating the levels of Business and Technology, capturing business knowledge independently of the underlying implementation technology. 

As with Open-EDI (NOTE:  ISO/IEC 14662 "Open‐edi reference model"), UMM separates the declarative modelling knowledge captures (the ‘what’) residing in the Business Operational View (BOV) that covers the business layer aspects such as business information, business agreements etc. from the procedural modelling knowledge captures (the ‘how’) residing in the Functional Service View (FSV) that covers the technology aspects. 

UMM is more of a BOV-centric methodology, defining three views in a model [7]: business domain view (BDV), business requirements view (BRV), and business transaction view (BTV).

Business collaborations are captured via specifying global choreographies, where each collaborating process of collaborating partners is specified as process orchestration. Hence, Collaboration choreographies are capturing all between business interactions and information (document) exchanges, setting the field ready for *contractual* level type of agreement definitions between collaborating business partners and organisations. 

UMM specifies a conceptual meta model and introduces a method on how to create Business Artefacts using UML constructs. UMM extends UML by introducing UML profiles, stereotypes, tagged values and constraints in OCL thereby supporting the creation of B2B and generally B2G choreographies. UMM uses Activity Diagrams of UML 2.2 extended with specific process profiles, to define stakeholder behaviour by describing the flow of actions of Actors – Stakeholders and their interactions with objects representing data.

![image alt text](_images/image_1.11.png)

**Figure 1-11: UMM Foundation Metamodels**

The UMM Foundation Metamodels involve the Business Requirements View, The Business Choreography View and the Business Information View. Each of these is further analysed in metamodel ‘packages’ (Figure 1.11), which use UML extensions of Use Case Diagrams and Activity Diagrams defined in the UMM profiles, stereotypes and tagged values. In the following a short presentation of the UMM collaboration business process packages is made. A full reference can be found in CEFACT related documentation (NOTE:  http://umm-dev.org/umm-specification/). As with ArchiMate, all metamodels have been cross-referenced, mapping similar concepts via a consistent naming scheme across methodologies by using references and links and subtyping. A more comprehensive set of models and the terminology (dictionary) used can be found in Appendix A2.

The UMM **Business Requirements View**( Figure 1.12) identifies the business processes in the domain and the business issues that are important to stakeholders (Business Drivers). The step captures intra‐organizational as well as inter‐organizational business processes. The business requirements view results in a description of the business domain and the related relevant business processes (*Business Process View*) which are captured as use cases. UMM models the dynamics (the behaviour) of each business process, using Activity Models, or Sequence Diagrams which are connected to the related Use Case describing the Business Process. A UMM Activity Model may be set to describe one or more processes for one or more Business Partners, whose profiles are described as Actors and Roles (*Business Partner View*). A UMM Sequence Diagram describes information exchanges between Business Partners. An Activity Model may be set to illustrate the state changes to Business Entities, and in UMM, Business Entities are "real‐word" objects (data, and information, captured in the *Business Entity View* of UMM, elaborated in CEFACT as Business Information Entities (BIE) and Aggregated Business Information Entities (ABIE)) shared among the collaborating Partners. 

![image alt text](_images/image_1.12.png)

**Figure 1-12: UMM Business Requirements View**

The UMM **Business Choreography View** (Figure 1.13) defines the global choreography between collaborating business partners collaborative business processes. Within the Business Choreography View, the *Business Transaction View* describes the requirements of *Business Transaction Use Cases* and their Actors/Participating Roles. Every Business Transaction is defined as a choreography where information is exchanged between collaborating business partners (modelled as Roles).

The **Business Collaboration View** captures the requirements of Business Collaboration Use Cases and the Authorized Collaborating Roles/Actors. When a Business Collaboration Use Case involves different parties, there may be different Realizations of the choreographies which are modeled within the *Business Realization View*.

The UMM **Business Information View **models the Business entities related to business transactions, covering the information exchanged between collaborating parties in business transactions. UMM is defining information exchanges taking a document-centric approach. A Business Information View contains Business Information Artifacts. The recommended modelling approach for Business Information is the Core Components Technical Specification (CCTS).

![image alt text](_images/image_1.13.png)

**Figure 1-13: Business Choreography View**

            1. BPMN Metamodels for Processes (M2).

BPMN is a very popular way of modeling processes in Business Domains, supporting different levels of abstraction in modelling processes and collaborations. BPMN models are considered as a standard way of describing processes and process flows in CORE. 

![image alt text](_images/image_1.14.png)

**Figure 1-14: BPMN Metamodel**

A metamodel for BPMN (M2) (Figure 1.14) has been introduced to describe the basic language constructs. Generally, Archimate and BPMN complement each other. ArchiMate provides a compact and cohesive way to capture and represent high-level processes, relations and context. BPMN excels in detailed workflow modeling, including sub-process and tasks. The BPMN specifications can be simulated and/or executed. On the other hand, BPMN does not provide the means to capture the concepts forming the higher Business and Motivations context as Archimate.

### Information Management Metamodels

The Common Warehouse Metamodel (CWM) of OMG [8] defines a common metamodel and an XML interchange format for meta data, to be used in Business Analysis and the data warehouse information exchanges. 

CWM is a complete metamodel providing both the syntax and semantics required to construct meta data describing the components of an Information Supply Chain. The CWM is composed of different (Figure 1.15) metamodels covering different subdomains of the Information Supply Chain. The Common Warehouse Metamodel is stable and has been adopted by vendors and customers for metadata interchange especially in relational databases. 

![image alt text](_images/image_1.15.png)

**Figure 1-15: CWM domains**

One aim of the Information Management Metamodel (IMM) [8] (Figure 1.16) is to bridge the gap between the UML, data and XML modelling worlds. The data modeling community still has quite a high level of resistance to the UML notation: the vision of IMM is to allow tools to easily switch between UML and the other notations. This will have significant benefits for improved communication between different communities (even database designers and developers in the same organization).

![image alt text](_images/image_1.16.png)

**Figure 1-16: IMM Metamodeling Domains**

IMM links to CWM, in essence taking over it, and consists of a set of metamodels that address business and technology view of Information management as well as aspects of model management. 

The IMM Core metamodel is MOF2 compliant, and contains packages, classes and associations that form core of the IMM standard and used by rest of the IMM metamodels. The IMM Core concepts enable the development of models of both structured, un-structured types of Information. 

IMM uses a hub-spoke approach for model transformations, providing a common language for mapping between metamodel and model transformation. 

The information Management Metamodels context covers the Transformations in Lineage operations in CORE, as well as the modelling for Information Visualisation and the technology modeling which is transparently covered in all Application Development tooling utilities developed in WP7. 

## Modelling in CORE for the living labs

In the Living Labs analysis, knowledge was captured and models in the M0 (modelling) level were created in the formalisms detailed in the previous section, that is, Archimate, BPMN, and UMM/UML. In the following, some of these models are illustrated, where Appendix A.3 includes a more comprehensive set.

## ArchiMate Model; Layer and Cross-Layer Models

Motivation Layer Models have been constructed for CORE and the Living Labs.

The motivation model in Figure 1.17 lays out several Objectives, Drivers, Measures, Principles and Requirements from the CORE innovation agenda.

![image alt text](_images/image_1.17.png)

**Figure 1-17: Motivation Model for CORE**

The Business Scenario of Flora Holland is illustrated in Figure 1.17, which shows as stated in the Business Analysis the Locations (KENYA, OMAN, SPAIN, BELGIUM, and Flora Holland Premises as well as the FF premises and Consolidation Centers), the modelled Business Functions (Flowers Packing), Business Collaborations (e.g. the implementation of transportation legs and Shipments) and Business Processes (loading, unloading, quality checks) and services by third parties.

![image alt text](_images/image_1.18.png)

**Figure 1-18: Business Scenario for Flora Holland**

Figure 1.19 Illustrates a model combining the Business layer, including Actors, Processes and Events, the Applications Layer, Including Services Interfaces and Application Components, and the Infrastructure (Technology) Layer, showing communications and internal systems (highly abstracted).

![image alt text](_images/image_1.19.png)

**Figure 1-19: Living Lab11 Business - Application - Technology Concepts**

